//
//  SelectCardIssuerViewController.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/11/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class SelectCardIssuerViewController: UIViewController, UIPickerViewDataSource,UIPickerViewDelegate {
    
    @IBOutlet weak var cardIssuerPicker: UIPickerView!
    @IBAction func unwindToCIVC(segue: UIStoryboardSegue) {
    }
    
    var cardIssuers = [CardIssuer]()
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cardIssuers.count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> CardIssuer? {
        return cardIssuers[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 32, y: 0, width: cardIssuerPicker.bounds.width-32, height: 80))
        
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 28, width: 48, height: 24))
        
        let pickerLabel = UILabel(frame: CGRect(x: 60,y: 0, width: cardIssuerPicker.bounds.width - 60, height: 80))
        let titleData = cardIssuers[row].name
        pickerLabel.text = titleData
        
        myImageView.image = cardIssuers[row].image
        myView.addSubview(pickerLabel)
        myView.addSubview(myImageView)
        
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        UserPaymentsInfo.sharedInstance.card_issuer = self.cardIssuers[row]
        
        performSegue(withIdentifier: "segueInstallments", sender: self)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.topItem?.title = "Ingrese banco"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = "Ingrese banco"
        
        cardIssuerPicker.dataSource = self
        cardIssuerPicker.delegate = self
        
        let params: Parameters = [
            "public_key": Constants.PUBLIC_KEY,
            "payment_method_id": UserPaymentsInfo.sharedInstance.payment_method!.id ?? ""
        ]
        
        let uri = "/card_issuers"
        
        print (Constants.API_ROUTE+uri)
        print (params)
        
        Alamofire.request(Constants.API_ROUTE+uri, parameters: params).responseArray { (response:  DataResponse<[CardIssuer]>) in
            if let cardIssuers = response.result.value {
                
                if (response.result.value?.count == 0) {
                    DispatchQueue.main.async {
                        let strMessage = "Disculpe, no hay opciones disponibles para \(UserPaymentsInfo.sharedInstance.payment_method!.name!) en este momento. Por favor elija otro medio de pago."
                        
                        let alertVC = UIAlertController(title: "No hay opciones", message: strMessage, preferredStyle: .alert)
                        
                        alertVC.addAction(UIAlertAction(title: "Volver", style: UIAlertActionStyle.cancel, handler: {
                            (alertAction: UIAlertAction!) in
                            alertVC.dismiss(animated: true, completion: nil)
                            self.performSegue(withIdentifier: "unwindToPMVC", sender: self)
                        }))
                        
                        self.present(alertVC, animated: true, completion: nil)
                    }
                }
                
                self.addItemsToView(cardIssuers: cardIssuers)
            }
        }
    }
    
    
    func addItemsToView(cardIssuers: [CardIssuer]) -> Void {
        
        self.cardIssuers = cardIssuers
        self.cardIssuerPicker.reloadAllComponents()
        
        for (cardIssuer) in cardIssuers {
            
            let imageUrl = URL(string: cardIssuer.thumbnail!)!
            
            let session = URLSession(configuration: .default)
            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: imageUrl) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            DispatchQueue.main.async {
                                cardIssuer.image = UIImage(data: imageData)
                                self.cardIssuerPicker.reloadAllComponents()
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
