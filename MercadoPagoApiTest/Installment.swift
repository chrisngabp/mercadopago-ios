//
//  Installment.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/11/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import Foundation
import ObjectMapper

class Installment: Mappable {
    var issuer: CardIssuer?
    var payer_costs: [PayerCost]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        issuer <- map["issuer"]
        payer_costs <- map ["payer_costs"]
    }
}

class PayerCost: Mappable {
    var recommended_message: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        recommended_message <- map["recommended_message"]
    }
}
