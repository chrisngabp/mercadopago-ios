//
//  ViewController.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/10/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var amountInput: UITextField!
    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Ingrese monto"
        
        if (UserPaymentsInfo.sharedInstance.has_finished) {
            
            DispatchQueue.main.async {
                let strMessage = "Los datos elegidos fueron: \n Monto: \(UserPaymentsInfo.sharedInstance.amount!)\n Medio de pago: \(UserPaymentsInfo.sharedInstance.payment_method!.name!) \n Banco: \(UserPaymentsInfo.sharedInstance.installment!.issuer!.name!) \n Cuotas: \(UserPaymentsInfo.sharedInstance.payer_cost!.recommended_message!)"
                
                self.present(UIAlertController(title: "Alert", message: strMessage, preferredStyle: .alert), animated: true, completion: nil)
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Ingrese monto"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Ingrese monto"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendAmount(_ sender: UIButton) {
        UserPaymentsInfo.sharedInstance.amount = amountInput.text
        
        performSegue(withIdentifier: "seguePaymentsMethod", sender: self)
    
    }

}

