//
//  CardIssuer.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/11/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import Foundation
import ObjectMapper

class CardIssuer: Mappable {
    var id: String?
    var name: String?
    var thumbnail: String?
    var image: UIImage?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        thumbnail <- map["thumbnail"]
        image <- map["image"]
    }
}
