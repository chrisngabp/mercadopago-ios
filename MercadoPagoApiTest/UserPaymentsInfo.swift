//
//  UserPaymentsInfo.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/10/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import Foundation

class UserPaymentsInfo {
    
    var amount: String?
    var payment_method: PaymentMethod?
    var card_issuer: CardIssuer?
    var installment: Installment?
    var payer_cost: PayerCost?
    var has_finished = false
    
    static let sharedInstance = UserPaymentsInfo()
}
