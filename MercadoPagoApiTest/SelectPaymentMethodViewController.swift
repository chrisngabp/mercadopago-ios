//
//  SelectPaymentMethodViewController.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/10/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class SelectPaymentMethodViewController: UIViewController, UIPickerViewDataSource,UIPickerViewDelegate {

    @IBOutlet weak var paymentMethodPicker: UIPickerView!
    @IBAction func unwindToPMVC(segue: UIStoryboardSegue) {
    }
    
    var paymentMethods = [PaymentMethod]()
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return paymentMethods.count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> PaymentMethod? {
        return paymentMethods[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 32, y: 0, width: paymentMethodPicker.bounds.width-32, height: 80))
        
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 28, width: 48, height: 24))
        
        let pickerLabel = UILabel(frame: CGRect(x: 60,y: 0, width: paymentMethodPicker.bounds.width - 60, height: 80))
        let titleData = paymentMethods[row].name
        pickerLabel.text = titleData
        
        myImageView.image = paymentMethods[row].image
        myView.addSubview(pickerLabel)
        myView.addSubview(myImageView)
        
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        UserPaymentsInfo.sharedInstance.payment_method = self.paymentMethods[row]
        
        performSegue(withIdentifier: "segueCardIssuers", sender: self)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.topItem?.title = "Ingrese tipo de pago"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = "Ingrese tipo de pago"
        
        paymentMethodPicker.dataSource = self
        paymentMethodPicker.delegate = self
        
        let params: Parameters = ["public_key": Constants.PUBLIC_KEY]

        Alamofire.request(Constants.API_ROUTE, parameters: params).responseArray { (response:  DataResponse<[PaymentMethod]>) in
            if let paymentMethods = response.result.value {
                self.addItemsToView(paymentMethods: paymentMethods)
            }
        }
    }
    
    func addItemsToView(paymentMethods: [PaymentMethod]) -> Void {
        
        self.paymentMethods = paymentMethods
        self.paymentMethodPicker.reloadAllComponents()
        
        for (paymentMethod) in paymentMethods {
            
            let imageUrl = URL(string: paymentMethod.thumbnail!)!
            
            let session = URLSession(configuration: .default)
            let downloadPicTask = session.dataTask(with: imageUrl) { (data, response, error) in
                if let e = error {
                    print("Error downloading picture: \(e)")
                } else {
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded picture with response code \(res.statusCode)")
                        if let imageData = data {
                            
                            DispatchQueue.main.async {
                                paymentMethod.image = UIImage(data: imageData)
                                self.paymentMethodPicker.reloadAllComponents()
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
