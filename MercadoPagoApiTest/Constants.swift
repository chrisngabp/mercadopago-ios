//
//  Utils.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/10/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import Foundation

struct Constants {
    static let API_ROUTE = "https://api.mercadopago.com/v1/payment_methods"
    static let PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88"
}
