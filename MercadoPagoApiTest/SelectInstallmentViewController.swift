//
//  SelectInstallmentViewController.swift
//  MercadoPagoApiTest
//
//  Created by Christian Panetta on 2/11/17.
//  Copyright © 2017 Christian Panetta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class SelectInstallmentViewController: UIViewController, UIPickerViewDataSource,UIPickerViewDelegate {

    @IBOutlet weak var installmentPicker: UIPickerView!
    var installments = [Installment]()
    var payer_costs = [PayerCost]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        installmentPicker.dataSource = self
        installmentPicker.delegate = self
        
        self.navigationController?.navigationBar.topItem?.title = "Ingrese pagos"
        
        let params: Parameters = [
            "public_key": Constants.PUBLIC_KEY,
            "amount": UserPaymentsInfo.sharedInstance.amount!,
            "payment_method_id": UserPaymentsInfo.sharedInstance.payment_method!.id ?? "",
            "issuer.id": UserPaymentsInfo.sharedInstance.card_issuer!.id ?? ""
        ]
        
        let uri = "/installments"
        
        Alamofire.request(Constants.API_ROUTE+uri, parameters: params).responseArray { (response:  DataResponse<[Installment]>) in

            if let installments = response.result.value {
                
                if (response.result.value?.count == 0) {
                    DispatchQueue.main.async {
                        let strMessage = "Disculpe, no hay opciones de cuotas disponibles en este momento. Por favor elija otro banco o modifique el monto."
                        
                        let alertVC = UIAlertController(title: "No hay opciones", message: strMessage, preferredStyle: .alert)
                        
                        alertVC.addAction(UIAlertAction(title: "Volver", style: UIAlertActionStyle.cancel, handler: {
                            (alertAction: UIAlertAction!) in
                            alertVC.dismiss(animated: true, completion: nil)
                            self.performSegue(withIdentifier: "unwindToCIVC", sender: self)
                        }))
                        
                        self.present(alertVC, animated: true, completion: nil)
                    }
                } else {
                    self.addItemsToView(installments: installments)
                }
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Ingrese pagos"
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return payer_costs.count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> PayerCost? {
        return payer_costs[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: installmentPicker.bounds.width, height: 80))
        let pickerLabel = UILabel(frame: CGRect(x: 32, y: 0, width: installmentPicker.bounds.width - 60, height: 80))
        let titleData = payer_costs[row].recommended_message
        pickerLabel.text = titleData
        myView.addSubview(pickerLabel)
        
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        for installment in self.installments {
            UserPaymentsInfo.sharedInstance.installment = installment
            UserPaymentsInfo.sharedInstance.payer_cost = installment.payer_costs![row]
        }
        
        UserPaymentsInfo.sharedInstance.has_finished = true
        
        performSegue(withIdentifier: "unwindToVC", sender: self)
    }
    
    
    func addItemsToView(installments: [Installment]) -> Void {
        self.installments = installments
        self.payer_costs = installments[0].payer_costs!
        
        DispatchQueue.main.async {
            self.installmentPicker.reloadAllComponents()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
